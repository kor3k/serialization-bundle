<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

class kor3kSerializationExtension extends ConfigurableExtension
{
    protected function loadInternal(array $config, ContainerBuilder $container): void
    {
        $container->setParameter('kor3k_serialization.generic.enabled', $generic = $config['generic']['enabled']);
        if ($generic) {
            $container->setParameter(
                'kor3k_serialization.generic.native.enabled',
                $config['generic']['serializer']['native']['enabled']
            );

            $container->setParameter(
                'kor3k_serialization.generic.custom.enabled',
                $config['generic']['serializer']['custom']['enabled']
            );
            $container->setParameter(
                'kor3k_serialization.generic.custom.service',
                $config['generic']['serializer']['custom']['service']
            );

            $container->setParameter(
                'kor3k_serialization.generic.symfony.enabled',
                $config['generic']['serializer']['symfony']['enabled']
            );
            $container->setParameter(
                'kor3k_serialization.generic.symfony.context',
                $config['generic']['serializer']['symfony']['context']
            );
        }

        $container->setParameter('kor3k_serialization.json.enabled', $json = $config['json']['enabled']);
        if ($json) {
            $container->setParameter(
                'kor3k_serialization.json.native.enabled',
                $config['json']['serializer']['native']['enabled']
            );

            $container->setParameter(
                'kor3k_serialization.json.custom.enabled',
                $config['json']['serializer']['custom']['enabled']
            );
            $container->setParameter(
                'kor3k_serialization.json.custom.service',
                $config['json']['serializer']['custom']['service']
            );

            $container->setParameter(
                'kor3k_serialization.json.symfony.enabled',
                $config['json']['serializer']['symfony']['enabled']
            );
            $container->setParameter(
                'kor3k_serialization.json.symfony.context',
                $config['json']['serializer']['symfony']['context']
            );
        }

        $container->setParameter('kor3k_serialization.csv.enabled', $csv = $config['csv']['enabled']);
        if ($csv) {
            $container->setParameter(
                'kor3k_serialization.csv.native.enabled',
                $config['csv']['serializer']['native']['enabled']
            );

            $container->setParameter(
                'kor3k_serialization.csv.custom.enabled',
                $config['csv']['serializer']['custom']['enabled']
            );
            $container->setParameter(
                'kor3k_serialization.csv.custom.service',
                $config['csv']['serializer']['custom']['service']
            );

            $container->setParameter(
                'kor3k_serialization.csv.symfony.enabled',
                $config['csv']['serializer']['symfony']['enabled']
            );
            $container->setParameter(
                'kor3k_serialization.csv.symfony.context',
                $config['csv']['serializer']['symfony']['context']
            );
        }

        $container->setParameter('kor3k_serialization.base64.enabled', $base64 = $config['base64']['enabled']);
        if ($base64) {
            $container->setParameter(
                'kor3k_serialization.base64.native.enabled',
                $config['base64']['serializer']['native']['enabled']
            );

            $container->setParameter(
                'kor3k_serialization.base64.custom.enabled',
                $config['base64']['serializer']['custom']['enabled']
            );
            $container->setParameter(
                'kor3k_serialization.base64.custom.service',
                $config['base64']['serializer']['custom']['service']
            );

            $container->setParameter(
                'kor3k_serialization.base64.symfony.enabled',
                $config['base64']['serializer']['symfony']['enabled']
            );
            $container->setParameter(
                'kor3k_serialization.base64.symfony.encoder',
                $config['base64']['serializer']['symfony']['encoder']
            );
        }

        $container->setParameter('kor3k_serialization.zip.enabled', $zip = $config['zip']['enabled']);
        if ($zip) {
            $container->setParameter(
                'kor3k_serialization.zip.native.enabled',
                $config['zip']['serializer']['native']['enabled']
            );

            $container->setParameter(
                'kor3k_serialization.zip.custom.enabled',
                $config['zip']['serializer']['custom']['enabled']
            );
            $container->setParameter(
                'kor3k_serialization.zip.custom.service',
                $config['zip']['serializer']['custom']['service']
            );

            $container->setParameter(
                'kor3k_serialization.zip.symfony.enabled',
                $config['zip']['serializer']['symfony']['enabled']
            );
        }
    }
}
