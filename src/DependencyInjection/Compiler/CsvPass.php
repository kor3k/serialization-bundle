<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\DependencyInjection\Compiler;

use kor3k\SerializationBundle\Serialization\Csv\CsvSerializer;
use kor3k\SerializationBundle\Serialization\Csv\CsvNativeSerializer;
use kor3k\SerializationBundle\Serialization\Csv\CsvSymfonySerializer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class CsvPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->getParameter('kor3k_serialization.csv.enabled')) {
            return;
        }

        if ($container->getParameter('kor3k_serialization.csv.native.enabled')) {
            $this->registerNative($container);
        }

        if ($container->getParameter('kor3k_serialization.csv.symfony.enabled')) {
            $this->registerSymfony($container);
        }

        if ($container->getParameter('kor3k_serialization.csv.custom.enabled')) {
            $this->registerCustom($container);
        }

        $container->setAlias(CsvSerializer::class, 'kor3k_serialization.csv');
    }

    private function registerNative(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(CsvNativeSerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
        ;

        $container->setDefinition('kor3k_serialization.csv.native', $serializer);
        $container->setAlias('kor3k_serialization.csv', 'kor3k_serialization.csv.native');
    }

    private function registerSymfony(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(CsvSymfonySerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
            ->setFactory([null, 'factory'])
            ->setArgument('$serializer', new Reference('serializer'))
        ;

        $context = $container->getParameter('kor3k_serialization.csv.symfony.context');
        if ($context) {
            $serializer->setArgument('$serializerContext', new Reference($context));
        }

        $container->setDefinition('kor3k_serialization.csv.symfony', $serializer);
        $container->setAlias('kor3k_serialization.csv', 'kor3k_serialization.csv.symfony');
    }

    private function registerCustom(ContainerBuilder $container): void
    {
        $service = $container->getParameter('kor3k_serialization.csv.custom.service');
        if (!$service) {
            throw new InvalidArgumentException(sprintf(
                '%s must be set if %s is true',
                'kor3k_serialization.csv.custom.service',
                'kor3k_serialization.csv.custom.enabled',
            ));
        }

        if (!\is_a($container->findDefinition($service)->getClass(), CsvSerializer::class, true)) {
            throw new InvalidArgumentException(sprintf(
                '%s service must implement %s',
                'kor3k_serialization.csv.custom.service',
                CsvSerializer::class,
            ));
        }

        $container->setAlias('kor3k_serialization.csv.custom', $service);
        $container->setAlias('kor3k_serialization.csv', 'kor3k_serialization.csv.custom');
    }
}
