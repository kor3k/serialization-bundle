<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\DependencyInjection\Compiler;

use kor3k\SerializationBundle\Serialization\Json\JsonSerializer;
use kor3k\SerializationBundle\Serialization\Json\JsonNativeSerializer;
use kor3k\SerializationBundle\Serialization\Json\JsonSymfonySerializer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class JsonPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->getParameter('kor3k_serialization.json.enabled')) {
            return;
        }

        if ($container->getParameter('kor3k_serialization.json.native.enabled')) {
            $this->registerNative($container);
        }

        if ($container->getParameter('kor3k_serialization.json.symfony.enabled')) {
            $this->registerSymfony($container);
        }

        if ($container->getParameter('kor3k_serialization.json.custom.enabled')) {
            $this->registerCustom($container);
        }

        $container->setAlias(JsonSerializer::class, 'kor3k_serialization.json');
    }

    private function registerNative(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(JsonNativeSerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
        ;

        $container->setDefinition('kor3k_serialization.json.native', $serializer);
        $container->setAlias('kor3k_serialization.json', 'kor3k_serialization.json.native');
    }

    private function registerSymfony(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(JsonSymfonySerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
            ->setFactory([null, 'factory'])
            ->setArgument('$serializer', new Reference('serializer'))
        ;

        $context = $container->getParameter('kor3k_serialization.json.symfony.context');
        if ($context) {
            $serializer->setArgument('$serializerContext', new Reference($context));
        }

        $container->setDefinition('kor3k_serialization.json.symfony', $serializer);
        $container->setAlias('kor3k_serialization.json', 'kor3k_serialization.json.symfony');
    }

    private function registerCustom(ContainerBuilder $container): void
    {
        $service = $container->getParameter('kor3k_serialization.json.custom.service');
        if (!$service) {
            throw new InvalidArgumentException(sprintf(
                '%s must be set if %s is true',
                'kor3k_serialization.json.custom.service',
                'kor3k_serialization.json.custom.enabled',
            ));
        }

        if (!\is_a($container->findDefinition($service)->getClass(), JsonSerializer::class, true)) {
            throw new InvalidArgumentException(sprintf(
                '%s service must implement %s',
                'kor3k_serialization.json.custom.service',
                JsonSerializer::class,
            ));
        }

        $container->setAlias('kor3k_serialization.json.custom', $service);
        $container->setAlias('kor3k_serialization.json', 'kor3k_serialization.json.custom');
    }
}
