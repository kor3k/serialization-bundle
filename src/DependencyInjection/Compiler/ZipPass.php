<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\DependencyInjection\Compiler;

use kor3k\SerializationBundle\Serialization\Zip\ZipSerializer;
use kor3k\SerializationBundle\Serialization\Zip\ZipNativeSerializer;
use kor3k\SerializationBundle\Serialization\Zip\ZipSymfonySerializer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class ZipPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->getParameter('kor3k_serialization.zip.enabled')) {
            return;
        }

        if ($container->getParameter('kor3k_serialization.zip.native.enabled')) {
            $this->registerNative($container);
        }

        if ($container->getParameter('kor3k_serialization.zip.symfony.enabled')) {
            $this->registerSymfony($container);
        }

        if ($container->getParameter('kor3k_serialization.zip.custom.enabled')) {
            $this->registerCustom($container);
        }

        $container->setAlias(ZipSerializer::class, 'kor3k_serialization.zip');
    }

    private function registerNative(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(ZipNativeSerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
        ;

        $container->setDefinition('kor3k_serialization.zip.native', $serializer);
        $container->setAlias('kor3k_serialization.zip', 'kor3k_serialization.zip.native');
    }

    private function registerSymfony(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(ZipSymfonySerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
        ;

        $container->setDefinition('kor3k_serialization.zip.symfony', $serializer);
        $container->setAlias('kor3k_serialization.zip', 'kor3k_serialization.zip.symfony');
    }

    private function registerCustom(ContainerBuilder $container): void
    {
        $service = $container->getParameter('kor3k_serialization.zip.custom.service');
        if (!$service) {
            throw new InvalidArgumentException(sprintf(
                '%s must be set if %s is true',
                'kor3k_serialization.zip.custom.service',
                'kor3k_serialization.zip.custom.enabled',
            ));
        }

        if (!\is_a($container->findDefinition($service)->getClass(), ZipSerializer::class, true)) {
            throw new InvalidArgumentException(sprintf(
                '%s service must implement %s',
                'kor3k_serialization.zip.custom.service',
                ZipSerializer::class,
            ));
        }

        $container->setAlias('kor3k_serialization.zip.custom', $service);
        $container->setAlias('kor3k_serialization.zip', 'kor3k_serialization.zip.custom');
    }
}
