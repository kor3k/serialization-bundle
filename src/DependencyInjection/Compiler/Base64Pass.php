<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\DependencyInjection\Compiler;

use kor3k\SerializationBundle\Serialization\Base64\Base64Serializer;
use kor3k\SerializationBundle\Serialization\Base64\Base64NativeSerializer;
use kor3k\SerializationBundle\Serialization\Base64\Base64SymfonySerializer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class Base64Pass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->getParameter('kor3k_serialization.base64.enabled')) {
            return;
        }

        if ($container->getParameter('kor3k_serialization.base64.native.enabled')) {
            $this->registerNative($container);
        }

        if ($container->getParameter('kor3k_serialization.base64.symfony.enabled')) {
            $this->registerSymfony($container);
        }

        if ($container->getParameter('kor3k_serialization.base64.custom.enabled')) {
            $this->registerCustom($container);
        }

        $container->setAlias(Base64Serializer::class, 'kor3k_serialization.base64');
    }

    private function registerNative(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(Base64NativeSerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
        ;

        $container->setDefinition('kor3k_serialization.base64.native', $serializer);
        $container->setAlias('kor3k_serialization.base64', 'kor3k_serialization.base64.native');
    }

    private function registerSymfony(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(Base64SymfonySerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
            ->setFactory([null, 'factory'])
        ;

        $encoder = $container->getParameter('kor3k_serialization.base64.symfony.encoder');
        if ($encoder) {
            $serializer->setArgument('$encoder', new Reference($encoder));
        }

        $container->setDefinition('kor3k_serialization.base64.symfony', $serializer);
        $container->setAlias('kor3k_serialization.base64', 'kor3k_serialization.base64.symfony');
    }

    private function registerCustom(ContainerBuilder $container): void
    {
        $service = $container->getParameter('kor3k_serialization.base64.custom.service');
        if (!$service) {
            throw new InvalidArgumentException(sprintf(
                '%s must be set if %s is true',
                'kor3k_serialization.base64.custom.service',
                'kor3k_serialization.base64.custom.enabled',
            ));
        }

        if (!\is_a($container->findDefinition($service)->getClass(), Base64Serializer::class, true)) {
            throw new InvalidArgumentException(sprintf(
                '%s service must implement %s',
                'kor3k_serialization.base64.custom.service',
                Base64Serializer::class,
            ));
        }

        $container->setAlias('kor3k_serialization.base64.custom', $service);
        $container->setAlias('kor3k_serialization.base64', 'kor3k_serialization.base64.custom');
    }
}
