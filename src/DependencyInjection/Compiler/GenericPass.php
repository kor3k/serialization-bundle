<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\DependencyInjection\Compiler;

use kor3k\SerializationBundle\Serialization\Generic\GenericSerializer;
use kor3k\SerializationBundle\Serialization\Generic\GenericNativeSerializer;
use kor3k\SerializationBundle\Serialization\Generic\GenericSymfonySerializer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class GenericPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->getParameter('kor3k_serialization.generic.enabled')) {
            return;
        }

        if ($container->getParameter('kor3k_serialization.generic.native.enabled')) {
            $this->registerNative($container);
        }

        if ($container->getParameter('kor3k_serialization.generic.symfony.enabled')) {
            $this->registerSymfony($container);
        }

        if ($container->getParameter('kor3k_serialization.generic.custom.enabled')) {
            $this->registerCustom($container);
        }

        $container->setAlias(GenericSerializer::class, 'kor3k_serialization.generic');
    }

    private function registerNative(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(GenericNativeSerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
        ;

        $container->setDefinition('kor3k_serialization.generic.native', $serializer);
        $container->setAlias('kor3k_serialization.generic', 'kor3k_serialization.generic.native');
    }

    private function registerSymfony(ContainerBuilder $container): void
    {
        $serializer = new Definition();
        $serializer
            ->setClass(GenericSymfonySerializer::class)
            ->setAutowired(true)
            ->setAutoconfigured(true)
            ->setFactory([null, 'factory'])
            ->setArgument('$serializer', new Reference('serializer'))
        ;

        $context = $container->getParameter('kor3k_serialization.generic.symfony.context');
        if ($context) {
            $serializer->setArgument('$serializerContext', new Reference($context));
        }

        $container->setDefinition('kor3k_serialization.generic.symfony', $serializer);
        $container->setAlias('kor3k_serialization.generic', 'kor3k_serialization.generic.symfony');
    }

    private function registerCustom(ContainerBuilder $container): void
    {
        $service = $container->getParameter('kor3k_serialization.generic.custom.service');
        if (!$service) {
            throw new InvalidArgumentException(sprintf(
                '%s must be set if %s is true',
                'kor3k_serialization.generic.custom.service',
                'kor3k_serialization.generic.custom.enabled',
            ));
        }

        if (!\is_a($container->findDefinition($service)->getClass(), GenericSerializer::class, true)) {
            throw new InvalidArgumentException(sprintf(
                '%s service must implement %s',
                'kor3k_serialization.generic.custom.service',
                GenericSerializer::class,
            ));
        }

        $container->setAlias('kor3k_serialization.generic.custom', $service);
        $container->setAlias('kor3k_serialization.generic', 'kor3k_serialization.generic.custom');
    }
}
