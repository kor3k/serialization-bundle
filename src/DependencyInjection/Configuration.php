<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('kor3k_serialization');

        $treeBuilder
            ->getRootNode()
            ->addDefaultsIfNotSet()
            ->append($this->generic())
            ->append($this->json())
            ->append($this->csv())
            ->append($this->base64())
            ->append($this->zip())
            ->end()
        ;

        return $treeBuilder;
    }

    private function generic(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('generic');

        $node = $treeBuilder
            ->getRootNode()
            ->addDefaultsIfNotSet()
            ->canBeEnabled()
            ->info('generic serializer')
            ->children()
                ->arrayNode('serializer')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('native')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                        ->end()
                        ->arrayNode('custom')
                            ->addDefaultsIfNotSet()
                            ->canBeEnabled()
                            ->children()
                                ->scalarNode('service')
                                    ->defaultNull()
                                    ->info('service id for custom generic serializer')
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('symfony')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                            ->children()
                                ->scalarNode('context')
                                    ->defaultNull()
                                    ->info('invokable service which returns Symfony\Component\Serializer\Context\ContextBuilderInterface')
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $node;
    }

    private function csv(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('csv');

        $node = $treeBuilder
            ->getRootNode()
            ->addDefaultsIfNotSet()
            ->canBeEnabled()
            ->info('csv serializer')
            ->children()
                ->arrayNode('serializer')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('native')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                        ->end()
                        ->arrayNode('custom')
                            ->addDefaultsIfNotSet()
                            ->canBeEnabled()
                            ->children()
                                ->scalarNode('service')
                                    ->defaultNull()
                                    ->info('service id for custom csv serializer')
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('symfony')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                            ->children()
                                ->scalarNode('context')
                                    ->defaultNull()
                                    ->info('invokable service which returns Symfony\Component\Serializer\Context\ContextBuilderInterface')
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $node;
    }

    private function json(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('json');

        $node = $treeBuilder
            ->getRootNode()
            ->addDefaultsIfNotSet()
            ->canBeEnabled()
            ->info('json serializer')
            ->children()
                ->arrayNode('serializer')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('native')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                        ->end()
                        ->arrayNode('custom')
                            ->addDefaultsIfNotSet()
                            ->canBeEnabled()
                            ->children()
                                ->scalarNode('service')
                                    ->defaultNull()
                                    ->info('service id for custom json serializer')
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('symfony')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                            ->children()
                                ->scalarNode('context')
                                    ->defaultNull()
                                    ->info('invokable service which returns Symfony\Component\Serializer\Context\ContextBuilderInterface')
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $node;
    }

    private function base64(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('base64');

        $node = $treeBuilder
            ->getRootNode()
            ->addDefaultsIfNotSet()
            ->canBeEnabled()
            ->info('base64 serializer')
            ->children()
                ->arrayNode('serializer')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('native')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                        ->end()
                        ->arrayNode('custom')
                            ->addDefaultsIfNotSet()
                            ->canBeEnabled()
                            ->children()
                                ->scalarNode('service')
                                    ->defaultNull()
                                    ->info('service id for custom base64 serializer')
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('symfony')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                            ->children()
                                ->scalarNode('encoder')
                                    ->defaultNull()
                                    ->info('invokable service which returns Symfony\Component\Mime\Encoder\ContentEncoderInterface')
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $node;
    }

    private function zip(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('zip');

        $node = $treeBuilder
            ->getRootNode()
            ->addDefaultsIfNotSet()
            ->canBeEnabled()
            ->info('zip serializer')
            ->children()
                ->arrayNode('serializer')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('native')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                        ->end()
                        ->arrayNode('custom')
                            ->addDefaultsIfNotSet()
                            ->canBeEnabled()
                            ->children()
                                ->scalarNode('service')
                                    ->defaultNull()
                                    ->info('service id for custom zip serializer')
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('symfony')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $node;
    }
}
