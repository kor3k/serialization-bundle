<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Json;

readonly class JsonContext
{
    public const DEFAULT_ENCODE_FLAGS = \JSON_THROW_ON_ERROR | \JSON_UNESCAPED_UNICODE | \JSON_UNESCAPED_SLASHES | \JSON_NUMERIC_CHECK | \JSON_PRESERVE_ZERO_FRACTION;
    public const DEFAULT_DECODE_FLAGS = \JSON_THROW_ON_ERROR | \JSON_BIGINT_AS_STRING;

    public function __construct(
        public int $encodeFlags = self::DEFAULT_ENCODE_FLAGS,
        public int $decodeFlags = self::DEFAULT_DECODE_FLAGS,
    ) {
    }

    public function withEncodeFlags(int $encodeFlags): self
    {
        return new self(encodeFlags: $encodeFlags, decodeFlags: $this->decodeFlags);
    }

    public function withDecodeFlags(int $decodeFlags): self
    {
        return new self(encodeFlags: $this->encodeFlags, decodeFlags: $decodeFlags);
    }
}
