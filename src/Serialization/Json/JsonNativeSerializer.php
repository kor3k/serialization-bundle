<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Json;

use kor3k\SerializationBundle\Serialization\Exception\JsonSerializationException as SerializationException;

class JsonNativeSerializer implements JsonSerializer
{
    public function encodeJson(array $data, JsonContext $context = new JsonContext()): string
    {
        try {
            return \json_encode(value: $data, flags: $context->encodeFlags);
        } catch (\JsonException $e) {
            throw SerializationException::serializationError(data: $data, previous: $e);
        }
    }

    public function decodeJson(string $json, JsonContext $context = new JsonContext()): array
    {
        try {
            return \json_decode(json: $json, associative: true, flags: $context->decodeFlags);
        } catch (\JsonException $e) {
            throw SerializationException::deserializationError(data: $json, previous: $e);
        }
    }
}
