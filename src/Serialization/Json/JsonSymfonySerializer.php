<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Json;

use kor3k\SerializationBundle\Serialization\Exception\JsonSerializationException as SerializationException;
use Symfony\Component\Serializer\Context\ContextBuilderInterface;
use Symfony\Component\Serializer\Context\Encoder\JsonEncoderContextBuilder;
use Symfony\Component\Serializer\Encoder\DecoderInterface as SymfonyDecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface as SymfonyEncoderInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface as SerializerException;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

class JsonSymfonySerializer implements JsonSerializer
{
    protected readonly JsonEncoderContextBuilder $serializerContext;

    public function __construct(
        /** @var SymfonyDecoderInterface&SymfonyEncoderInterface */
        protected readonly SymfonySerializerInterface $serializer,
        ContextBuilderInterface $serializerContext = new JsonEncoderContextBuilder(),
    ) {
        $this->serializerContext = (new JsonEncoderContextBuilder())
            ->withContext($serializerContext)
            ->withAssociative(true);
    }

    public static function factory(SymfonySerializerInterface $serializer, ?callable $serializerContext = null): self
    {
        $serializerContext ??= fn () => new JsonEncoderContextBuilder();

        return new self($serializer, $serializerContext());
    }

    public function encodeJson(array $data, JsonContext $context = new JsonContext()): string
    {
        try {
            return $this->serializer->encode(
                data: $data,
                format: 'json',
                context: $this->serializerContext->withEncodeOptions($context->encodeFlags)->toArray(),
            );
        } catch (SerializerException $e) {
            throw SerializationException::serializationError(data: $data, previous: $e);
        }
    }

    public function decodeJson(string $json, JsonContext $context = new JsonContext()): array
    {
        try {
            return $this->serializer->decode(
                data: $json,
                format: 'json',
                context: $this->serializerContext->withDecodeOptions($context->decodeFlags)->toArray(),
            );
        } catch (SerializerException $e) {
            throw SerializationException::deserializationError(data: $json, previous: $e);
        }
    }
}
