<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Json;

use kor3k\SerializationBundle\Serialization\Exception\Exception as SerializationException;

/**
 * this de/serializes array of data to/from "associative" json.
 */
interface JsonSerializer
{
    /**
     * @throws SerializationException
     */
    public function encodeJson(array $data, JsonContext $context = new JsonContext()): string;

    /**
     * @throws SerializationException
     */
    public function decodeJson(string $json, JsonContext $context = new JsonContext()): array;
}
