<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip;

use kor3k\SerializationBundle\Serialization\Zip\Decoder\ZipDecoder;
use kor3k\SerializationBundle\Serialization\Zip\Encoder\ZipEncoder;

interface ZipSerializer
{
    public function createZipEncoder(\SplFileInfo $target, ZipContext $context = new ZipContext()): ZipEncoder;

    public function createZipDecoder(\SplFileInfo $source, ZipContext $context = new ZipContext()): ZipDecoder;
}
