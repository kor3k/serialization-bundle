<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip;

use kor3k\SerializationBundle\Serialization\Zip\Decoder\ZipDecoder;
use kor3k\SerializationBundle\Serialization\Zip\Decoder\ZipNativeDecoder;
use kor3k\SerializationBundle\Serialization\Zip\Encoder\ZipEncoder;
use kor3k\SerializationBundle\Serialization\Zip\Encoder\ZipNativeEncoder;

class ZipNativeSerializer implements ZipSerializer
{

    public function createZipEncoder(\SplFileInfo $target, ZipContext $context = new ZipContext()): ZipEncoder
    {
        return new ZipNativeEncoder(file: $target, context: $context);
    }

    public function createZipDecoder(\SplFileInfo $source, ZipContext $context = new ZipContext()): ZipDecoder
    {
        return new ZipNativeDecoder(file: $source, context: $context);
    }
}
