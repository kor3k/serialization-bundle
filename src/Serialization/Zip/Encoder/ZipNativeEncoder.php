<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip\Encoder;

use kor3k\SerializationBundle\Serialization\Exception\ZipSerializationException as SerializationException;
use kor3k\SerializationBundle\Serialization\Zip\ZipContext;

class ZipNativeEncoder implements ZipEncoder
{
    private readonly \ZipArchive $zip;

    public function __construct(
        private readonly \SplFileInfo $file,
        public readonly ZipContext $context,
    ) {
        $this->zip = new \ZipArchive();
        $this->openZip();
    }

    private function openZip(): void
    {
        try {
            \touch($this->file->getPathname());

            $open = $this->zip->open( // open for editing. if size is 0, create & overwrite
                $this->file->getRealPath(),
                \ZipArchive::CHECKCONS | ($this->file->getSize() ? 0 : (\ZipArchive::CREATE | \ZipArchive::OVERWRITE)),
            );

            !$open && throw new \RuntimeException(sprintf('Error opening Zip file for writing: "%s"', $this->file->getPathname()));
        } catch (\Throwable $e) {
            throw new SerializationException(message: $e->getMessage(), previous: $e);
        }
    }

    private function closeZip(): void
    {
        try {
            $this->zip->close();
        } catch (\ValueError $e) {
        } catch (\Throwable $e) {
            throw new SerializationException(message: $e->getMessage(), previous: $e);
        }
    }

    public function addFile(string $source, string $target): void
    {
        !\is_file($source) && throw new SerializationException(data: $source, message: 'This is not a file');

        try {
            $add = $this->zip->addFile(
                filepath: $source,
                entryname: $target,
                flags: \ZipArchive::FL_OVERWRITE | \ZipArchive::FL_ENC_UTF_8
            );
            !$add && throw new \RuntimeException(sprintf('Error adding file to zip: "%s"', $source));
        } catch (\Throwable $e) {
            throw new SerializationException(data: $target, message: $e->getMessage(), previous: $e);
        }
    }

    public function addDir(string $source, string $target): void
    {
        if (!\is_dir($source)) {
            throw new SerializationException(data: $source, message: 'This is not a directory');
        }

        $addDir = function (string $source, string $target, callable $addDir) {
            $this->zip->addEmptyDir(dirname: $target, flags: \ZipArchive::FL_ENC_UTF_8);
            $content = \array_diff(\scandir($source), ['.', '..']);

            foreach ($content as $entry) {
                $sourcePath = "$source/$entry";
                $targetPath = "$target/$entry";

                if (\is_file($sourcePath) && \is_link($sourcePath)) {
                    $this->addFile(\readlink($sourcePath), $targetPath);
                } elseif (\is_file($sourcePath)) {
                    $this->addFile($sourcePath, $targetPath);
                } elseif (\is_dir($sourcePath)) {
                    $addDir($sourcePath, $targetPath, $addDir);
                }
            }
        };

        $addDir($source, $target, $addDir);
    }

    /**
     * @param resource|string $source
     */
    public function addData(mixed $source, string $target): void
    {
        !\is_string($source) && !\is_resource($source)
            && throw new SerializationException(data: $source, message: 'This is not a stream|string');

        try {
            $add = $this->zip->addFromString(
                name: $target,
                content: \is_resource($source) ? \stream_get_contents($source) : $source,
                flags: \ZipArchive::FL_OVERWRITE | \ZipArchive::FL_ENC_UTF_8
            );
            !$add && throw new \RuntimeException(sprintf('Error adding file to zip: "%s"', $source));
        } catch (\Throwable $e) {
            throw new SerializationException(data: $target, message: $e->getMessage(), previous: $e);
        }
    }

    public function deleteEntry(string $target): void
    {
        try {
            $this->zip->deleteName(name: $target);
        } catch (\Throwable $e) {
            throw new SerializationException(data: $target, message: $e->getMessage(), previous: $e);
        }
    }

    public function commit(): void
    {
        $this->closeZip();
        $this->openZip();
    }

    public function reset(): void
    {
        $this->zip->unchangeAll();
    }

    public function __destruct()
    {
        $this->closeZip();
    }
}
