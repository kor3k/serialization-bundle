<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip\Encoder;

use kor3k\SerializationBundle\Serialization\Exception\ZipSerializationException as SerializationException;

interface ZipEncoder
{
    /**
     * @throws SerializationException
     */
    public function addFile(string $source, string $target): void;

    /**
     * @throws SerializationException
     */
    public function addDir(string $source, string $target): void;

    /**
     * @throws SerializationException
     */
    public function addData(mixed $source, string $target): void;

    /**
     * @throws SerializationException
     */
    public function deleteEntry(string $target): void;

    /**
     * @throws SerializationException
     */
    public function commit(): void;

    public function reset(): void;
}
