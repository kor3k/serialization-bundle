<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip\Encoder;

use kor3k\SerializationBundle\Serialization\Exception\ZipSerializationException as SerializationException;
use kor3k\SerializationBundle\Serialization\Zip\ZipContext;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ZipSymfonyEncoder implements ZipEncoder
{
    private readonly ZipHelper $helper;

    public function __construct(
        private readonly \SplFileInfo $file,
        public readonly ZipContext $context,
        private readonly Filesystem $fs = new Filesystem(),
    ) {
        $this->helper = new ZipHelper(tempDir: $this->context->tempDir, fs: $this->fs);
    }

    public function addFile(string $source, string $target): void
    {
        $this->helper->addFile($source, $target);
    }

    public function addDir(string $source, string $target): void
    {
        $this->helper->addDir($source, $target);
    }

    public function addData(mixed $source, string $target): void
    {
        $this->helper->addData($source, $target);
    }

    public function deleteEntry(string $target): void
    {
        $this->helper->deleteEntry($target);
    }

    private function doDelete(): void
    {
        if (!$this->fs->exists($this->file->getPathname())) {
            return;
        }

        if (!$this->helper->hasDelete()) {
            return;
        }

        $target = $this->file->getRealPath();

        $process = ['zip', '-r', '-d'];
        $process = [...$process, $target];

        foreach ($this->helper->toDelete() as $item) {
            try {
                (new Process([...$process, $item]))->mustRun();
            } catch (ProcessFailedException $e) {
                if (\str_contains($e->getMessage(), 'probably not a zip file')) {
                    break;
                } elseif (\str_contains($e->getMessage(), 'name not matched')) {
                    continue;
                } else {
                    throw new SerializationException(message: $e->getMessage(), previous: $e);
                }
            }
        }

        $this->helper->resetDelete();
    }

    private function doUpdate(): void
    {
        if (!$this->helper->hasUpdate()) {
            return;
        }

        $this->helper->flush();

        $target = $this->file->getPathname();

        $process = ['zip', '-r', '-X', '-9', '-u'];
        $process = [...$process, $target, '.'];
        $process = new Process($process, $this->helper->getDir());

        try {
            $process->mustRun();
            $this->helper->resetUpdate();
        } catch (ProcessFailedException $e) {
            throw new SerializationException(message: $e->getMessage(), previous: $e);
        }
    }

    public function commit(): void
    {
        if ($this->fs->exists($this->file->getPathname()) && !$this->file->getSize()) {
            $this->fs->remove($this->file->getPathname());
        }

        $this->doDelete();
        $this->doUpdate();
        $this->reset();
    }

    public function reset(): void
    {
        $this->helper->reset();
    }
}
