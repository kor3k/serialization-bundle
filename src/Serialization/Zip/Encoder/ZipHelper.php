<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip\Encoder;

use kor3k\SerializationBundle\Serialization\Exception\ZipSerializationException as SerializationException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class ZipHelper
{
    private readonly string $dir;

    /**
     * @var array{string: string} target => source
     */
    private array $dirs = [];
    /**
     * @var array{string: string} target => source
     */
    private array $files = [];
    /**
     * @var array{string: resource|string} target => source
     */
    private array $data = [];
    /**
     * @var array{string: void} target => void
     */
    private array $delete = [];

    public function __construct(
        string $tempDir,
        private readonly Filesystem $fs = new Filesystem(),
    ) {
        do {
            $dir = \sprintf('%s/%s', $tempDir, \bin2hex(\random_bytes(10)));
        } while (\file_exists($dir));

        $this->dir = $dir;
    }

    public function getDir(): string
    {
        return $this->dir;
    }

    public function hasDelete(): bool
    {
        return !empty($this->delete);
    }

    public function hasUpdate(): bool
    {
        return !empty($this->dirs) || !empty($this->files) || !empty($this->data);
    }

    public function resetUpdate(): void
    {
        $this->dirs = [];
        $this->files = [];
        $this->data = [];
    }

    public function resetDelete(): void
    {
        $this->delete = [];
    }

    public function reset(): void
    {
        $this->resetUpdate();
        $this->resetDelete();
        $this->fs->remove($this->dir);
    }

    public function addFile(string $source, string $target): void
    {
        if (\is_link($source)) {
            $source = \readlink($source);
        }

        !\is_file($source) && throw new SerializationException(data: $source, message: 'This is not a file');

        $this->files[$target] = $source;
        unset($this->delete[$target]);
    }

    public function addDir(string $source, string $target): void
    {
        if (\is_link($source)) {
            $source = \readlink($source);
        }

        !\is_dir($source) && throw new SerializationException(data: $source, message: 'This is not a directory');

        $this->dirs[$target] = $source;
        unset($this->delete[$target]);
    }

    public function addData(mixed $source, string $target): void
    {
        !\is_string($source) && !\is_resource($source)
        && throw new SerializationException(data: $source, message: 'This is not a stream|string');

        $this->data[$target] = $source;
        unset($this->delete[$target]);
    }

    public function deleteEntry(string $target): void
    {
        unset($this->files[$target]);
        unset($this->dirs[$target]);
        unset($this->data[$target]);

        $this->delete[$target] = true;
    }

    public function flush(): void
    {
        $this->fs->mkdir($this->dir);

        foreach ($this->dirs as $target => $source) {
            $this->fs->mirror(originDir: $source, targetDir: "$this->dir/$target", options: ['override' => true]);
        }

        foreach ($this->files as $target => $source) {
            $this->fs->copy(originFile: $source, targetFile: "$this->dir/$target", overwriteNewerFiles: true);
        }

        foreach ($this->data as $target => $source) {
            $targetPath = "$this->dir/$target";

            if (\is_resource($source)) {
                $fp = \fopen($targetPath, 'w');
                \stream_copy_to_stream($source, $fp);
                \fclose($fp);
                unset($fp);
            } else {
                \file_put_contents($targetPath, $source);
            }
        }
    }

    public function toDelete(): iterable
    {
        return array_keys($this->delete);
    }

    public function __destruct()
    {
        $this->fs->remove($this->dir);
    }
}
