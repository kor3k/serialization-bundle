<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip;

readonly class ZipContext
{
    public function __construct(
        public string $tempDir = '/tmp',
    ) {

    }
}
