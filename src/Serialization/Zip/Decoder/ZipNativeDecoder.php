<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip\Decoder;

use kor3k\SerializationBundle\Serialization\Exception\ZipSerializationException as SerializationException;
use kor3k\SerializationBundle\Serialization\Zip\ZipContext;

class ZipNativeDecoder implements ZipDecoder
{
    private \ZipArchive $zip;

    public function __construct(
        private readonly \SplFileInfo $file,
        public readonly ZipContext $context,
    ) {
        try {
            \clearstatcache(true);
            !$this->file->getRealPath() && throw new \RuntimeException(\sprintf('File does not exist: "%s"', $this->file->getPathname()));

            $this->zip = new \ZipArchive();
            $open = $this->zip->open($this->file->getRealPath(), \ZipArchive::RDONLY | \ZipArchive::CHECKCONS);

            !$open && throw new \RuntimeException(\sprintf('Error opening Zip file for reading: "%s"', $this->file->getRealPath()));
        } catch (\Throwable $e) {
            throw new SerializationException(message: $e->getMessage(), previous: $e);
        }
    }

    public function count(): int
    {
        return \count($this->zip);
    }

    public function extractAll(string $dir): void
    {
        $result = $this->zip->extractTo($dir);
        !$result && throw SerializationException::deserializationError(data: $this->file->getPathname());
    }

    public function extract(string $dir, string ...$paths): void
    {
        $result = $this->zip->extractTo($dir, $paths);
        !$result && throw SerializationException::deserializationError(data: $this->file->getPathname());
    }

    public function extractAs(string $target, string $source): void
    {
        try {
            $sourceStream = $this->zip->getStreamName($source);
            !$sourceStream && throw new SerializationException(data: $source, message: 'Cannot open ZIP stream');

            @\mkdir(directory: \dirname($target), recursive: true);
            $targetStream = \fopen($target, 'w');
            !$targetStream && throw new SerializationException(data: $target, message: 'Cannot open file stream');

            $result = \stream_copy_to_stream($sourceStream, $targetStream);
            !$result && throw new SerializationException(message: 'Cannot copy stream');
        } finally {
            try {
                \fclose($sourceStream ?? null);
                \fclose($targetStream ?? null);
            } catch (\Throwable $e) {
            }
        }
    }

    public function __destruct()
    {
        $this->zip->close();
    }
}
