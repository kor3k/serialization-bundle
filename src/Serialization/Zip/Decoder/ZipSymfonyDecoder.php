<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip\Decoder;

use kor3k\SerializationBundle\Serialization\Exception\ZipSerializationException as SerializationException;
use kor3k\SerializationBundle\Serialization\Zip\ZipContext;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ZipSymfonyDecoder implements ZipDecoder
{
    public function __construct(
        private readonly \SplFileInfo $file,
        public readonly ZipContext $context,
        private readonly Filesystem $fs = new Filesystem(),
    ) {
        try {
            \clearstatcache(true);
            !$this->file->getRealPath() && throw new \RuntimeException(\sprintf('File does not exist: "%s"', $this->file->getPathname()));
        } catch (\Throwable $e) {
            throw new SerializationException(message: $e->getMessage(), previous: $e);
        }
    }

    public function count(): int
    {
         zipinfo  -h <file name>  |  tr '\n' ':'  | awk -F':'  '{print $2 , $5 , "files"}'
        $process = Process::fromShellCommandline(
            command: <<<'CMD'
            zipinfo -h "${:ZIPFILE}"  |  tr '\n' ':'  | awk -F':'  '{print $5}'
            CMD,
            env: ['ZIPFILE' => $this->file->getRealPath()],
        );

        try {
            return (int) $process->mustRun()->getOutput();
        } catch (ProcessFailedException $e) {
            throw new SerializationException(message: $e->getMessage(), previous: $e);
        }
    }

    public function extractAll(string $dir): void
    {
        $this->fs->mkdir($dir);

        $process = ['unzip', '-o'];
        $process = [...$process, $this->file->getRealPath()];
        $process = [...$process, '-d', $dir];

        $process = new Process($process);

        try {
            $process->mustRun();
        } catch (ProcessFailedException $e) {
            throw new SerializationException(message: $e->getMessage(), previous: $e);
        }
    }

    public function extract(string $dir, string ...$paths): void
    {
        $this->fs->mkdir($dir);

        $process = ['unzip', '-o'];
        $process = [...$process, $this->file->getRealPath()];
        $process = [...$process, ...$paths];
        $process = [...$process, '-d', $dir];

        $process = new Process($process);

        try {
            $process->mustRun();
        } catch (ProcessFailedException $e) {
            throw new SerializationException(message: $e->getMessage(), previous: $e);
        }
    }

    public function extractAs(string $target, string $source): void
    {
        $tmp = $this->context->tempDir;

        $this->extract($tmp, $source);

        $path = "$tmp/$source";
        if (\is_dir($path)) {
            $this->fs->mirror(originDir: $path, targetDir: $target, options: ['override' => true]);
        } elseif (\is_file($path)) {
            $this->fs->copy(originFile: $path, targetFile: $target, overwriteNewerFiles: true);
        }
    }
}
