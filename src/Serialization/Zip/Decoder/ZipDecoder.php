<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Zip\Decoder;

use kor3k\SerializationBundle\Serialization\Exception\ZipSerializationException as SerializationException;

interface ZipDecoder extends \Countable
{
    /**
     * @throws SerializationException
     */
    public function extractAll(string $dir): void;

    /**
     * @throws SerializationException
     */
    public function extract(string $dir, string ...$paths): void;

    /**
     * @throws SerializationException
     */
    public function extractAs(string $target, string $source): void;
}
