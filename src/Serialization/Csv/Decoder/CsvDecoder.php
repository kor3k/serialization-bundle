<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Decoder;

use kor3k\SerializationBundle\Serialization\Exception\Exception as SerializationException;

/**
 * @throws SerializationException
 */
interface CsvDecoder extends \SeekableIterator, \Countable
{

}
