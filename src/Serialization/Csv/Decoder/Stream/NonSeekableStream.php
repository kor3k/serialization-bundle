<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Decoder\Stream;

use kor3k\SerializationBundle\Serialization\Csv\CsvContext;

/**
 * @internal
 */
class NonSeekableStream implements ReadStream
{
    private ?int $count = null;
    private ?array $headers = null;
    private int $offset = 0;

    public function __construct(private readonly \SplFileInfo $file, private readonly CsvContext $context, bool $withHeaders = true)
    {
        $withHeaders && $this->initHeaders();
        $this->rewind();
    }

    private function createStream(): \SplFileObject
    {
        return StreamFactory::createFileStream($this->file, $this->context);
    }

    private function initHeaders(): void
    {
        $stream = $this->createStream();
        $headers = $stream->current();

        if (!$headers) {
            throw new \UnexpectedValueException('headers should be array, false returned. invalid csv.');
        }

        $this->headers = $headers;

        unset($stream);
    }

    private function initCount(): void
    {
        $stream = $this->createStream();

        $count = 0;
        while (!$stream->eof()) {
            ++$count;
            $stream->next();
        }

        if ($this->headers) {
            --$count;
        }

        $this->count = $count;

        unset($stream);
    }

    public function count(): int
    {
        if (null === $this->count) {
            $this->initCount();
        }

        return $this->count;
    }

    public function seek(int $offset): void
    {
        if ($this->headers) {
            ++$offset;
        }

        $this->offset = $offset;
    }

    public function rewind(): void
    {
        $this->seek(0);
    }

    public function getIterator(): \Iterator
    {
        $stream = $this->createStream();

        if ($this->offset) {
            for ($i = 0; $i <= $this->offset; $i++) {
                $stream->next();
            }
        }

        return new \NoRewindIterator($this->createIterator($stream, $this->headers));
    }

    private function createIterator(\SplFileObject $stream, ?array $headers): \Iterator
    {
        while ($stream->current()) {
            $row = $stream->current();
            if ($row) {
                try {
                    if ($headers) {
                        yield \array_combine($headers, $row);
                    } else {
                        yield $row;
                    }
                } catch (\ValueError $e) {
                    yield [];
                }
            }
            $stream->next();
        }
    }
}
