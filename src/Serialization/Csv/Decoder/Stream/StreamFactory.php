<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Decoder\Stream;

use kor3k\SerializationBundle\Serialization\Csv\CsvContext;

/**
 * @internal
 */
class StreamFactory
{
    public static function create(\SplFileInfo $file, CsvContext $context, bool $withHeaders = true): ReadStream
    {
        $stream = $file instanceof \SplFileObject ? $file : self::createFileStream($file, $context);

        try {
            $stream->seek(0);
            return new SeekableStream($stream, $withHeaders);
        } catch (\RuntimeException $e) {
            return new NonSeekableStream($stream, $context, $withHeaders);
        }
    }

    public static function createFileStream(\SplFileInfo $file, CsvContext $context): \SplFileObject
    {
        $stream = $file->openFile('r');
        $flags = \SplFileObject::READ_AHEAD | \SplFileObject::DROP_NEW_LINE | \SplFileObject::READ_CSV;
        if ($context->skipEmpty) {
            $flags = $flags | \SplFileObject::SKIP_EMPTY;
        }

        $stream->setFlags($flags);
        $stream->setCsvControl(
            separator: $context->delimiter,
            enclosure: $context->enclosure,
            escape: '',
        );

        return $stream;
    }
}
