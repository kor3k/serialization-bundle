<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Decoder\Stream;

interface ReadStream extends \IteratorAggregate, \Countable
{
    public function seek(int $offset): void;
    public function rewind(): void;

    public function getIterator(): \Iterator;
}
