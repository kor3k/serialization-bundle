<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Decoder\Stream;

/**
 * @internal
 */
class SeekableStream implements ReadStream
{
    private readonly int $count;
    private ?array $headers = null;

    public function __construct(private readonly \SplFileObject $stream, bool $withHeaders = true)
    {
        $withHeaders && $this->initHeaders();
        $this->initCount();
        $this->rewind();
    }

    private function initHeaders(): void
    {
        $this->stream->seek(0);
        $headers = $this->stream->current();

        if (!$headers) {
            throw new \UnexpectedValueException('headers should be array, false returned. invalid csv.');
        }

        $this->headers = $headers;
    }

    private function initCount(): void
    {
        $count = \max(0, \iterator_count($this->stream));
        if ($this->headers) {
            --$count;
        }

        $this->count = $count;
    }

    public function rewind(): void
    {
        $this->seek(0);
    }

    public function count(): int
    {
        return $this->count;
    }

    public function seek(int $offset): void
    {
        if ($this->headers) {
            ++$offset;
        }

        $this->stream->seek($offset);
    }

    public function getIterator(): \Iterator
    {
        return new \NoRewindIterator($this->createIterator($this->stream, $this->headers));
    }

    private function createIterator(\SplFileObject $stream, ?array $headers): \Iterator
    {
        while ($stream->valid()) {
            $row = $stream->current();
            if ($row) {
                try {
                    if ($headers) {
                        yield \array_combine($headers, $row);
                    } else {
                        yield $row;
                    }
                } catch (\ValueError $e) {
                    yield [];
                }
            }
            $stream->next();
        }
    }
}
