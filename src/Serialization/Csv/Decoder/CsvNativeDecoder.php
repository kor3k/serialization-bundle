<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Decoder;

use kor3k\SerializationBundle\Serialization\Csv\Decoder\Stream\ReadStream;
use kor3k\SerializationBundle\Serialization\Csv\Decoder\Stream\StreamFactory;
use kor3k\SerializationBundle\Serialization\Csv\CsvContext;
use kor3k\SerializationBundle\Serialization\Exception\CsvSerializationException as SerializationException;

class CsvNativeDecoder implements CsvDecoder
{
    private readonly ReadStream $stream;
    private \Iterator $iterator;
    private int $offset;

    public function __construct(
        public readonly \SplFileInfo $file,
        public readonly CsvContext $context,
        bool $withHeaders = true,
    ) {
        try {
            $this->stream = StreamFactory::create($this->file, $this->context, $withHeaders);
        } catch (\Throwable $e) {
            throw SerializationException::deserializationError($this->file->getPathname(), $e);
        }

        $this->seek(0);
    }

    public function count(): int
    {
        try {
            return $this->stream->count();
        } catch (\Throwable $e) {
            throw SerializationException::deserializationError($this->file->getPathname(), $e);
        }
    }

    /**
     * rewinds current "slice" only.
     * to rewind stream, use seek(0).
     *
     * @throws SerializationException
     */
    public function rewind(): void
    {
        try {
            $this->stream->seek($this->offset);
            $this->iterator = $this->stream->getIterator();
        } catch (\Throwable $e) {
            throw SerializationException::deserializationError($this->file->getPathname(), $e);
        }
    }

    /**
     * creates a "slice" of stream.
     * call seek(0) to reset.
     *
     * @throws SerializationException
     */
    public function seek(int $offset): void
    {
        $this->offset = $offset;
        $this->rewind();
    }

    public function current(): mixed
    {
        if ($this->context->utf8bom) {
            $current = $this->iterator->current();
            $removeBom = static fn ($v) => \str_replace(CsvContext::UTF8_BOM, '', $v);
            return \array_combine(
                keys: \array_map($removeBom, \array_keys($current)),
                values: \array_map($removeBom, \array_values($current)),
            );
        }

        return $this->iterator->current();
    }

    public function next(): void
    {
        $this->iterator->next();
    }

    public function key(): mixed
    {
        return $this->iterator->key();
    }

    public function valid(): bool
    {
        return $this->iterator->valid();
    }
}
