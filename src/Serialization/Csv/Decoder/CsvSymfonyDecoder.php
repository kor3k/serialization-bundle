<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Decoder;

use kor3k\SerializationBundle\Serialization\Csv\CsvContext;
use kor3k\SerializationBundle\Serialization\Exception\CsvSerializationException as SerializationException;
use Symfony\Component\Serializer\Context\Encoder\CsvEncoderContextBuilder;
use Symfony\Component\Serializer\Encoder\DecoderInterface as SymfonyDecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface as SymfonyEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

class CsvSymfonyDecoder implements CsvDecoder
{
    private \Iterator $iterator;
    private int $offset;
    private \ArrayIterator $data;

    public function __construct(
        public readonly \SplFileInfo $file,
        public readonly CsvContext $context,
        /** @var SymfonyDecoderInterface&SymfonyEncoderInterface */
        protected readonly SymfonySerializerInterface $serializer,
        protected CsvEncoderContextBuilder $serializerContext = new CsvEncoderContextBuilder(),
        bool $withHeaders = true,
    ) {
        $this->serializerContext = $this->serializerContext
            ->withEscapeChar('')
            ->withDelimiter($this->context->delimiter)
            ->withEnclosure($this->context->enclosure)
            ->withEndOfLine($this->context->eol)
            ->withOutputUtf8Bom($this->context->utf8bom)
            ->withNoHeaders(!$withHeaders)
        ;

        $this->init();
        $this->seek(0);
    }

    /**
     * @throws SerializationException
     */
    private function init(): void
    {
        try {
            $stream = $this->file instanceof \SplFileObject ? $this->file : $this->file->openFile('r');

            $flags = \SplFileObject::READ_AHEAD | \SplFileObject::DROP_NEW_LINE | \SplFileObject::READ_CSV;
            if ($this->context->skipEmpty) {
                $flags = $flags | \SplFileObject::SKIP_EMPTY;
            }
            $stream->setFlags($flags);

            \ob_start();
            $stream->fpassthru();
            $data = \ob_get_contents();
            \ob_end_clean();

            unset($stream);

            $this->data = new \ArrayIterator($this->serializer->decode(
                data: $data,
                format: 'csv',
                context: $this->serializerContext->toArray()),
            );
        } catch (\Throwable $e) {
            throw SerializationException::deserializationError($this->file->getPathname(), $e);
        }
    }

    public function count(): int
    {
        return \count($this->data);
    }

    /**
     * creates a "slice" of stream.
     * call seek(0) to reset.
     */
    public function seek(int $offset): void
    {
        $this->offset = $offset;
        $this->rewind();
    }

    /**
     * rewinds current "slice" only.
     * to rewind stream, use seek(0).
     */
    public function rewind(): void
    {
        $this->data->rewind();

        $iterator = new \LimitIterator($this->data, $this->offset);
        $iterator->rewind();
        $iterator = new \NoRewindIterator($iterator);

        $this->iterator = $iterator;
    }

    public function current(): mixed
    {
        return $this->iterator->current();
    }

    public function next(): void
    {
        $this->iterator->next();
    }

    public function key(): mixed
    {
        return $this->iterator->key();
    }

    public function valid(): bool
    {
        return $this->iterator->valid();
    }
}
