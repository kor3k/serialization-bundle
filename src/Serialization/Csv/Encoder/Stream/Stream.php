<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Encoder\Stream;

use kor3k\SerializationBundle\Serialization\Csv\CsvContext;

/**
 * @internal
 */
class Stream implements WriteStream
{
    public function __construct(private readonly \SplFileObject $stream, private readonly CsvContext $context)
    {
    }

    public function writeData(iterable $data): void
    {
        $result = $this->stream->fputcsv(
            fields: \iterator_to_array($data),
            separator: $this->context->delimiter,
            enclosure: $this->context->enclosure,
            eol: $this->context->eol,
            escape: '',
        );

        if (!$result) {
            throw new \RuntimeException('error writing csv data');
        }
    }

    public function writeRaw(string $data): void
    {
        $result = $this->stream->fwrite($data);

        if (!$result) {
            throw new \RuntimeException('error writing raw data');
        }
    }

    public function isAtStart(): bool
    {
        if (0 === $this->stream->ftell()) {
            return true;
        }

        return false;
    }

    public function flush(): void
    {
        $this->stream->fflush();
    }
}
