<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Encoder\Stream;

interface WriteStream
{
    public function writeData(iterable $data): void;
    public function writeRaw(string $data): void;
    public function isAtStart(): bool;
    public function flush(): void;
}
