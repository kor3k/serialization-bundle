<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Encoder\Stream;

use kor3k\SerializationBundle\Serialization\Csv\CsvContext;

/**
 * @internal
 */
class StreamFactory
{
    public static function create(\SplFileInfo $file, CsvContext $context): WriteStream
    {
        $stream = $file instanceof \SplFileObject ? $file : self::createFileStream($file, $context);

        try {
            $stream->seek(\PHP_INT_MAX);
        } catch (\RuntimeException $e) {
        }

        return new Stream($stream, $context);
    }

    public static function createFileStream(\SplFileInfo $file, CsvContext $context): \SplFileObject
    {
        $stream = $file->openFile('a+');
        // TODO: needed?
        $stream->setCsvControl(
            separator: $context->delimiter,
            enclosure: $context->enclosure,
            escape: '',
        );

        return $stream;
    }
}
