<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Encoder;

use kor3k\SerializationBundle\Serialization\Exception\Exception as SerializationException;

interface CsvEncoder
{
    /**
     * @throws SerializationException
     */
    public function writeData(iterable $data): void;

    /**
     * set headers derived from data & write data.
     * this method is inefficient as it converts $data to array.
     * it is recommended to call setHeaders() & writeData() instead.
     *
     * @throws SerializationException
     */
    public function write(iterable $data): void;

    /**
     * sets headers. writes them ONLY if not already written (i.e. if file is empty).
     */
    public function setHeaders(?iterable $headers): void;
}
