<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Encoder;

use kor3k\SerializationBundle\Serialization\Csv\Encoder\Stream\StreamFactory;
use kor3k\SerializationBundle\Serialization\Csv\Encoder\Stream\WriteStream;
use kor3k\SerializationBundle\Serialization\Csv\CsvContext;
use kor3k\SerializationBundle\Serialization\Csv\Encoder\CsvEncoder;
use kor3k\SerializationBundle\Serialization\Exception\CsvSerializationException as SerializationException;

class CsvNativeEncoder implements CsvEncoder
{
    private readonly WriteStream $stream;
    private ?array $headers = null;

    public function __construct(
        \SplFileInfo $file,
        public readonly CsvContext $context,
    ) {
        $this->stream = StreamFactory::create($file, $this->context);
    }

    private function writeBom(): void
    {
        // if stream is not at start, do not write anything
        if (!$this->stream->isAtStart()) {
            return;
        }

        // same if bom is not enabled
        if (!$this->context->utf8bom) {
            return;
        }

        try {
            $this->stream->writeRaw(CsvContext::UTF8_BOM);
        } catch (\Throwable $e) {
            throw SerializationException::serializationError('BOM', $e);
        }
    }

    private function writeHeaders(): void
    {
        // if stream is not at start, do not write anything
        if (!$this->stream->isAtStart()) {
            return;
        }

        // same if no headers are set
        if (null === $this->headers) {
            return;
        }

        try {
            $this->stream->writeData($this->headers);
        } catch (\Throwable $e) {
            throw SerializationException::serializationError($this->headers, $e);
        }
    }

    public function writeData(iterable $data): void
    {
        $this->writeBom();
        $this->writeHeaders();

        try {
            foreach ($data as $row) {
                $this->stream->writeData($row);
            }

            $this->stream->flush();
        } catch (\Throwable $e) {
            throw SerializationException::serializationError($data, $e);
        }
    }

    public function write(iterable $data): void
    {
        $data = \iterator_to_array($data);
        foreach ($data as $row) {
            $this->setHeaders(\array_keys($row));
            break;
        }

        $this->writeData($data);
    }

    public function setHeaders(?iterable $headers): void
    {
        $this->headers = $headers ? \iterator_to_array($headers) : null;
    }
}
