<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv\Encoder;

use kor3k\SerializationBundle\Serialization\Csv\Encoder\Stream\StreamFactory;
use kor3k\SerializationBundle\Serialization\Csv\Encoder\Stream\WriteStream;
use kor3k\SerializationBundle\Serialization\Csv\CsvContext;
use kor3k\SerializationBundle\Serialization\Csv\Encoder\CsvEncoder;
use kor3k\SerializationBundle\Serialization\Exception\CsvSerializationException as SerializationException;
use Symfony\Component\Serializer\Context\Encoder\CsvEncoderContextBuilder;
use Symfony\Component\Serializer\Encoder\DecoderInterface as SymfonyDecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface as SymfonyEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

class CsvSymfonyEncoder implements CsvEncoder
{
    private readonly WriteStream $stream;
    private ?array $headers = null;

    public function __construct(
        public readonly \SplFileInfo $file,
        public readonly CsvContext $context,
        /** @var SymfonyDecoderInterface&SymfonyEncoderInterface */
        protected readonly SymfonySerializerInterface $serializer,
        protected CsvEncoderContextBuilder $serializerContext = new CsvEncoderContextBuilder(),
    ) {
        $this->serializerContext = $this->serializerContext
            ->withEscapeChar('')
            ->withDelimiter($this->context->delimiter)
            ->withEnclosure($this->context->enclosure)
            ->withEndOfLine($this->context->eol)
            ->withOutputUtf8Bom($this->context->utf8bom)
        ;

        $this->stream = StreamFactory::create($file, $this->context);
    }

    private function writeHeaders(): void
    {
        $writeHeaders = true;

        // if stream is not at start, do not write anything
        if (!$this->stream->isAtStart()) {
            $writeHeaders = false;
        }

        // same if no headers are set
        if (null === $this->headers) {
            $writeHeaders = false;
        }

        if ($writeHeaders) {
            $this->serializerContext = $this->serializerContext->withHeaders(\iterator_to_array($this->headers))->withNoHeaders(false);
        } else {
            $this->serializerContext = $this->serializerContext->withHeaders(null)->withNoHeaders(true);
        }
    }

    public function writeData(iterable $data): void
    {
        $this->writeHeaders();

        try {
            $this->stream->writeRaw($this->serializer->encode(
                data: \iterator_to_array($data),
                format: 'csv',
                context: $this->serializerContext->toArray()),
            );

            $this->stream->flush();
        } catch (\Throwable $e) {
            throw SerializationException::serializationError($data, $e);
        }
    }

    public function write(iterable $data): void
    {
        $data = \iterator_to_array($data);
        foreach ($data as $row) {
            $this->setHeaders(\array_keys($row));
            break;
        }

        $this->writeData($data);
    }

    public function setHeaders(?iterable $headers): void
    {
        $this->headers = $headers ? \iterator_to_array($headers) : null;
    }
}
