<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv;

use kor3k\SerializationBundle\Serialization\Csv\Decoder\CsvDecoder;
use kor3k\SerializationBundle\Serialization\Csv\Decoder\CsvSymfonyDecoder;
use kor3k\SerializationBundle\Serialization\Csv\Encoder\CsvEncoder;
use kor3k\SerializationBundle\Serialization\Csv\Encoder\CsvSymfonyEncoder;
use Symfony\Component\Serializer\Context\ContextBuilderInterface;
use Symfony\Component\Serializer\Context\Encoder\CsvEncoderContextBuilder;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

class CsvSymfonySerializer implements CsvSerializer
{
    protected readonly CsvEncoderContextBuilder $serializerContext;

    public function __construct(
        protected readonly SymfonySerializerInterface $serializer,
        ContextBuilderInterface $serializerContext = new CsvEncoderContextBuilder(),
    ) {
        $this->serializerContext = (new CsvEncoderContextBuilder())->withContext($serializerContext);
    }

    public static function factory(SymfonySerializerInterface $serializer, ?callable $serializerContext = null): self
    {
        $serializerContext ??= fn () => new CsvEncoderContextBuilder();

        return new self($serializer, $serializerContext());
    }

    public function createCsvEncoder(\SplFileInfo $target, CsvContext $context = new CsvContext()): CsvEncoder
    {
        return new CsvSymfonyEncoder(
            file: $target,
            context: $context,
            serializer: $this->serializer,
            serializerContext: $this->serializerContext,
        );
    }

    public function createCsvDecoder(\SplFileInfo $source, CsvContext $context = new CsvContext(), bool $withHeaders = true): CsvDecoder
    {
        return new CsvSymfonyDecoder(
            file: $source,
            context: $context,
            serializer: $this->serializer,
            serializerContext: $this->serializerContext,
            withHeaders: $withHeaders,
        );
    }
}
