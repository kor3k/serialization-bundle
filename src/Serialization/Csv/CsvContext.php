<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv;

readonly class CsvContext
{
    public const UTF8_BOM = "\xEF\xBB\xBF";

    public function __construct(
        public string $delimiter = ";",
        public string $enclosure = "\"",
        public string $eol = "\n",
        public bool $skipEmpty = true,
        public bool $utf8bom = false,
    ) {
    }

    public function withDelimiter(string $delimiter): self
    {
        return new self(
            delimiter: $delimiter,
            enclosure: $this->enclosure,
            eol: $this->eol,
            skipEmpty: $this->skipEmpty,
            utf8bom: $this->utf8bom,
        );
    }

    public function withEnclosure(string $enclosure): self
    {
        return new self(
            delimiter: $this->delimiter,
            enclosure: $enclosure,
            eol: $this->eol,
            skipEmpty: $this->skipEmpty,
            utf8bom: $this->utf8bom,
        );
    }

    public function withEol(string $eol): self
    {
        return new self(
            delimiter: $this->delimiter,
            enclosure: $this->enclosure,
            eol: $eol,
            skipEmpty: $this->skipEmpty,
            utf8bom: $this->utf8bom,
        );
    }

    public function withSkipEmpty(bool $skipEmpty): self
    {
        return new self(
            delimiter: $this->delimiter,
            enclosure: $this->enclosure,
            eol: $this->eol,
            skipEmpty: $skipEmpty,
            utf8bom: $this->utf8bom,
        );
    }
}
