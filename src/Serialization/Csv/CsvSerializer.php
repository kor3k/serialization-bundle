<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv;

use kor3k\SerializationBundle\Serialization\Csv\Decoder\CsvDecoder;
use kor3k\SerializationBundle\Serialization\Csv\Encoder\CsvEncoder;

interface CsvSerializer
{
    public function createCsvEncoder(\SplFileInfo $target, CsvContext $context = new CsvContext()): CsvEncoder;

    public function createCsvDecoder(\SplFileInfo $source, CsvContext $context = new CsvContext(), bool $withHeaders = true): CsvDecoder;
}
