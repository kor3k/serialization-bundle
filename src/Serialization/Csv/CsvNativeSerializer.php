<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Csv;

use kor3k\SerializationBundle\Serialization\Csv\Decoder\CsvDecoder;
use kor3k\SerializationBundle\Serialization\Csv\Decoder\CsvNativeDecoder;
use kor3k\SerializationBundle\Serialization\Csv\Encoder\CsvEncoder;
use kor3k\SerializationBundle\Serialization\Csv\Encoder\CsvNativeEncoder;

class CsvNativeSerializer implements CsvSerializer
{
    public function createCsvEncoder(\SplFileInfo $target, CsvContext $context = new CsvContext()): CsvEncoder
    {
        return new CsvNativeEncoder(file: $target, context: $context);
    }

    public function createCsvDecoder(\SplFileInfo $source, CsvContext $context = new CsvContext(), bool $withHeaders = true): CsvDecoder
    {
        return new CsvNativeDecoder(file: $source, context: $context, withHeaders: $withHeaders);
    }
}
