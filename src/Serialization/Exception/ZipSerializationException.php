<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Exception;

class ZipSerializationException extends SerializationException
{
    public static function serializationError(mixed $data, ?\Throwable $previous = null): self
    {
        return new self(data: $data, message: 'Error serializing data to ZIP', previous: $previous);
    }

    public static function deserializationError(mixed $data, ?\Throwable $previous = null): self
    {
        return new self(data: $data, message: 'Error deserializing data from ZIP', previous: $previous);
    }
}
