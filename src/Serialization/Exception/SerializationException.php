<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Exception;

class SerializationException extends \RuntimeException implements Exception
{
    public function __construct(private readonly mixed $data = null, string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public static function serializationError(mixed $data, ?\Throwable $previous = null): self
    {
        return new self(data: $data, message: 'Error serializing data', previous: $previous);
    }

    public static function deserializationError(mixed $data, ?\Throwable $previous = null): self
    {
        return new self(data: $data, message: 'Error deserializing data', previous: $previous);
    }
}
