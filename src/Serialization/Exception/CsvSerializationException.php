<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Exception;

class CsvSerializationException extends SerializationException
{
    public static function serializationError(mixed $data, ?\Throwable $previous = null): self
    {
        return new self(data: $data, message: 'Error serializing data to CSV', previous: $previous);
    }

    public static function deserializationError(mixed $data, ?\Throwable $previous = null): self
    {
        return new self(data: $data, message: 'Error deserializing data from CSV', previous: $previous);
    }
}
