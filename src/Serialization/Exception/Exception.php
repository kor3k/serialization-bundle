<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Exception;

interface Exception extends \Throwable
{
}
