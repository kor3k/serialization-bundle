<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Generic;

use kor3k\SerializationBundle\Serialization\Exception\SerializationException;
use Symfony\Component\Serializer\Context\ContextBuilderInterface;
use Symfony\Component\Serializer\Context\SerializerContextBuilder;
use Symfony\Component\Serializer\Encoder\DecoderInterface as SymfonyDecoderInterface;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

class GenericSymfonySerializer implements GenericSerializer
{
    protected readonly ContextBuilderInterface $serializerContext;

    public function __construct(
        /** @var SymfonyDecoderInterface */
        protected readonly SymfonySerializerInterface $serializer,
        ContextBuilderInterface $serializerContext = new SerializerContextBuilder(),
    ) {
        $this->serializerContext = (new SerializerContextBuilder())->withContext($serializerContext);
    }

    public static function factory(SymfonySerializerInterface $serializer, ?callable $serializerContext = null): self
    {
        $serializerContext ??= fn () => new SerializerContextBuilder();

        return new self($serializer, $serializerContext());
    }

    public function serialize(mixed $data): string
    {
        try {
            return $this->serializer->serialize(
                data: $data,
                format: 'json',
                context: $this->serializerContext->toArray(),
            );
        } catch (\Throwable $e) {
            throw SerializationException::serializationError($data, $e);
        }
    }

    public function deserialize(string $data, ?string $class): mixed
    {
        try {
            if (!$class) {
                return $this->serializer->decode(data: $data, format: 'json', context: $this->getContext());
            }

            return $this->serializer->deserialize(
                data: $data,
                type: $class,
                format: 'json',
                context: $this->serializerContext->toArray(),
            );
        } catch (\Throwable $e) {
            throw SerializationException::deserializationError($data, $e);
        }
    }
}
