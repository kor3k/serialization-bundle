<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Generic;

use kor3k\SerializationBundle\Serialization\Exception\SerializationException;

class GenericNativeSerializer implements GenericSerializer
{
    public function serialize(mixed $data): string
    {
        try {
            return \serialize($data);
        } catch (\Throwable $e) {
            throw SerializationException::serializationError($data, $e);
        }
    }

    public function deserialize(string $data, ?string $class): mixed
    {
        try {
            if (!$class) {
                return \unserialize($data);
            }

            $object = \unserialize($data);
            if (!$object instanceof $class) {
                throw new \UnexpectedValueException(\sprintf('object should be of %s but is %s', $class, \get_class($object)));
            }

            return $object;
        } catch (\Throwable $e) {
            throw SerializationException::deserializationError($data, $e);
        }
    }
}
