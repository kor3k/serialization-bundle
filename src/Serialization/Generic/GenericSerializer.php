<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Generic;

use kor3k\SerializationBundle\Serialization\Exception\Exception as SerializationException;

/**
 * this serializer MUST handle all common data types: object|array|string|int|float|boolean|null.
 * it's main purpose is to be used by php, so php's serialize() function is just fine.
 */
interface GenericSerializer
{
    /**
     * @throws SerializationException
     */
    public function serialize(mixed $data): string;

    /**
     * @throws SerializationException
     */
    public function deserialize(string $data, ?string $class): mixed;
}
