<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Base64;

use kor3k\SerializationBundle\Serialization\Exception\Base64SerializationException as SerializationException;

class Base64NativeSerializer implements Base64Serializer
{
    public function encodeBase64(mixed $data): string
    {
        try {
            $data = match (true) {
                \is_resource($data) => \stream_get_contents($data),
                \is_string($data) => $data,
                $data instanceof \SplFileInfo => \file_get_contents($data->getRealPath()),
                default => throw new \InvalidArgumentException('data must be string|resource|SplFileInfo'),
            };

            return \base64_encode($data);
        } catch (\Throwable $e) {
            throw SerializationException::serializationError($data, $e);
        }
    }

    public function decodeBase64(string $data): string
    {
        try {
            return \base64_decode($data, true) ?: throw new \UnexpectedValueException('data could not be decoded');
        } catch (\Throwable $e) {
            throw SerializationException::deserializationError($data, $e);
        }
    }
}
