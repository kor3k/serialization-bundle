<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Base64;

use kor3k\SerializationBundle\Serialization\Exception\Base64SerializationException as SerializationException;
use Symfony\Component\Mime\Encoder\Base64ContentEncoder;
use Symfony\Component\Mime\Encoder\ContentEncoderInterface;

class Base64SymfonySerializer implements Base64Serializer
{
    public function __construct(protected ContentEncoderInterface $encoder = new Base64ContentEncoder())
    {
    }

    public static function factory(?callable $encoder = null): self
    {
        $encoder ??= fn () => new Base64ContentEncoder();

        return new self($encoder());
    }

    public function encodeBase64(mixed $data): string
    {
        try {
            return match (true) {
                $data instanceof \SplFileInfo => $this->encoder->encodeString(\file_get_contents($data->getRealPath())),
                \is_string($data) => $this->encoder->encodeString($data),
                \is_resource($data) => \implode('', \iterator_to_array($this->encoder->encodeByteStream($data))),
                default => throw new \InvalidArgumentException('data must be string|resource|SplFileInfo'),
            };
        } catch (\Throwable $e) {
            throw SerializationException::serializationError($data, $e);
        }
    }

    public function decodeBase64(string $data): string
    {
        try {
            return \base64_decode($data, true) ?: throw new \UnexpectedValueException('data could not be decoded');
        } catch (\Throwable $e) {
            throw SerializationException::deserializationError($data, $e);
        }
    }
}
