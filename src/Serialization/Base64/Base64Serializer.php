<?php declare(strict_types=1);

namespace kor3k\SerializationBundle\Serialization\Base64;

use kor3k\SerializationBundle\Serialization\Exception\Exception as SerializationException;

interface Base64Serializer
{
    /**
     * this MUST be able to read from string and stream (resource).
     *
     * @param resource|string $data
     *
     * @throws SerializationException
     */
    public function encodeBase64(mixed $data): string;

    /**
     * @throws SerializationException
     */
    public function decodeBase64(string $data): string;
}
