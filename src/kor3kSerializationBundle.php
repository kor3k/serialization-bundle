<?php declare(strict_types=1);

namespace kor3k\SerializationBundle;

use kor3k\SerializationBundle\DependencyInjection\Compiler;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class kor3kSerializationBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new Compiler\GenericPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 2);
        $container->addCompilerPass(new Compiler\JsonPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 2);
        $container->addCompilerPass(new Compiler\Base64Pass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 2);
        $container->addCompilerPass(new Compiler\CsvPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 2);
        $container->addCompilerPass(new Compiler\ZipPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 2);
    }
}

